#ifndef NET_INCLUDED
#define NET_INCLUDED

#define PORT                    8888
#define SENDFILE_RETRY_TIMES    10

int net_listen(int port);
void setnonblocking(int fd);
int net_sendfile(int out_fd, char *filename, off_t offset, size_t count);

#endif
