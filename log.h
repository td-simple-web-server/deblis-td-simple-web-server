#ifndef LOG_INCLUDED
#define LOG_INCLUDED 1

#define LOG_TYPE_ACCESS         1
#define LOG_TYPE_ERROR          2

#define LOG_FILE_LINE_SIZE      2048
#define LOG_DIR                 "logs/"
#define LOG_FILE_ACCESS         "access.log"
#define LOG_FILE_ERROR          "error.log"


int log2file(char *host, char *method, char *path, char *proto,
             int code, char *refer,
             int size, char *agent, char *clientip);


#endif
