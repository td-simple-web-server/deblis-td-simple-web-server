#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/sendfile.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>

#include "net.h"

void setnonblocking(int fd)
{
        int opts;
        if ((opts = fcntl(fd, F_GETFL)) < 0) {
                fprintf(stderr, "fcntl failed\n");
                return;
        }

        opts = opts | O_NONBLOCK;

        if (fcntl(fd, F_SETFL, opts) < 0) {
                fprintf(stderr, "fcntl failed\n");
                return;
        }

        return;
}

int net_listen(int port)
{
        int sfd, opt = 1;
        struct sockaddr_in sin;

        if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) <= 0) {
                perror("Socket failed\n");
                exit (-1);
        }

        setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (const void*)&opt, sizeof(opt));

        memset(&sin, 0, sizeof(struct sockaddr_in));

        sin.sin_family = AF_INET;
        sin.sin_port = htons((short)((port ? port : PORT)));
        sin.sin_addr.s_addr = INADDR_ANY;

        if (bind(sfd, (struct sockaddr *)&sin, sizeof(sin)) != 0) {
                perror("bind failed\n");
                exit (-1);
        }

        if (listen(sfd, 1024) != 0) {
                perror("listen failed\n");
                exit (-1);
        }

        return sfd;
}

/* Send file using system call: sendfile
 * Return 0 is send ok, 1 is buffer full, send again, continue wait event
 */
int net_sendfile(int out_fd, char *filename, off_t offset, size_t count)
{
        int fd, n = 0, err = 0;

        int no = 1;
        setsockopt(out_fd, IPPROTO_TCP, TCP_CORK, (char*)&no, sizeof(int));

        if ((fd = open(filename, O_RDONLY)) == -1) {
                fprintf(stderr, "Can not open file: %s\n", filename);
                return -1;
        }

        printf("Count is : %d, offset is :%d\n", count, offset);
        int write_num;

        for (;;) {
                write_num = sendfile(out_fd, fd, &offset, 8192);
                printf("%d\n", write_num);
                if((write_num == -1 && errno == EAGAIN) || write_num = 0)
                        break;
                offset += write_num;
        }
        /*
        while (n++ < SENDFILE_RETRY_TIMES &&
               (err = sendfile(out_fd, fd, offset, count)) == -1 &&
               errno == EAGAIN);
        if (err == -1 && errno != EPIPE) {
                fprintf(stderr, "Send file error: %s, %s, %d, %d\n", filename,
                        __FILE__, __LINE__, errno);
                close(fd);
                return -1;
        }
        */

        close(fd);
        return 0;
}
