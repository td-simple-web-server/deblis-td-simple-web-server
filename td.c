#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <signal.h>

#include "http.h"
#include "net.h"
#include "event.h"
#include "signal.h"
#include "mime.h"

char *root_dir;
int threads;

static void get_cores();

static void init_daemon()
{
        return;

        int pid;

        if ((pid = fork()))
                exit(0);
        else if (pid < 0)
                exit(1);

        if (setsid() == -1)
                exit(1);

        signal(SIGHUP, SIG_IGN);
        signal(SIGPIPE, SIG_IGN);

        if((pid = fork()))
                exit(0);
        else if (pid < 0)
                exit(1);

        chdir(root_dir);
        /*
        for (i = 0; i < 64; ++i)
                close(i);
                */

        open("/dev/null", O_RDONLY);
        open("/dev/null", O_RDWR);
        open("/dev/null", O_RDWR);

        umask(0);
        return;
}


int main(int argc, char *argv[])
{
        root_dir = getcwd(root_dir, 512);

        if (argc > 1) {
                threads = atoi(argv[1]);
                threads = threads < 0 ? 2 : threads;
        }

        sig_handler();

        init_daemon();
        get_cores();
        init_mime_set();

        main_poll_loop(net_listen(0));

        return 0;
}

static void get_cores()
{
        FILE *fp;
        char buf[256]; 
        threads = 0;

        if ((fp = fopen("/proc/cpuinfo", "r")) == NULL) {
                threads = 1;
                return;
        }

        while (fgets(buf, 255, fp)) 
                if (strstr(buf, "processor"))
                        ++threads;

        threads = threads ? threads : 1;
}
