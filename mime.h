#ifndef MIME_INCLUDED
#define MIME_INCLUDED

#include "lib/uthash.h"

#define MIME_TEXT_HTML  "html"
#define MIME_TYPE_FILE  "./mime.types"
#define MIME_SET_SIZE   255

typedef struct {
        char key[32];
        int id;
        char mime[64];
        UT_hash_handle hh;
} mime_set;

int init_mime_set();
mime_set * get_mimetype(char *key);

#endif
