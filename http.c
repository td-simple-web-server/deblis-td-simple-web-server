#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <time.h>

#include "http.h"
#include "event.h"
#include "net.h"
#include "log.h"
#include "mime.h"

extern char *root_dir;

static int parse_uri(_http_header_data *hhd)
{
        char *file;
        int size, qs = 0, ext = 0, upath = 0, path = 0;

        size = strlen(hhd->path);
        char t[size + 1];

        memset(t, 0, size + 1);
        strncpy(t, hhd->path, size);
        strncpy(hhd->ppath, t, size);

        int i;
        int ext_size = sizeof(hhd->ext),
            upath_size = sizeof(hhd->upath),
            path_size = sizeof(hhd->path),
            file_size = sizeof(hhd->file),
            qs_size = sizeof(hhd->qs);

        memset(hhd->ppath, 0, sizeof(hhd->ppath));
        memset(hhd->path, 0, path_size);
        memset(hhd->upath, 0, upath_size);
        memset(hhd->ext, 0, ext_size);
        memset(hhd->file, 0, file_size);
        memset(hhd->qs, 0, qs_size);

        for (i = 0; i < size; ++i) {
                if (t[i] == '?') {
                        qs = 1;
                        memset(hhd->qs, 0, qs_size);
                        strncpy(hhd->qs, &t[i] + 1, qs_size);
                        break;
                }

                if (ext && ext < ext_size && t[i] != '/') {
                        hhd->ext[ext - 1] = t[i];
                        ++ext;
                        continue;
                }

                if (ext && t[i] == '/') {
                        hhd->upath[0] = '/';
                        upath = 1;
                        ext = 0;
                        continue;
                }

                if (upath && upath < upath_size) {
                        hhd->upath[upath++] = t[i];
                        continue;
                }

                if (t[i] == '.') {
                        ext = 1;
                        continue;
                }

                /* path & host */
                if (i < path_size - 1) {
                        hhd->path[path++] = t[i];
                }
        }

        int end = 0;
        size = strlen(hhd->path);
        if (size > 0) {
                if ((file = strrchr(hhd->path, '/')) == NULL ||
                    (file == &hhd->path[size - 1])) {
                        end = 1;
                        file = "index";
                        strcpy(hhd->ext, "html");
                }
                strncpy(hhd->file, end ? file : file + 1,
                        MIN(strlen(file),file_size));
                
                if (!end) 
                        for (i = file - hhd->path + 1; i < size; i++)
                                hhd->path[i] = '\0';
        }

        /*
        printf("QS:%s...EXT:%s...File:%s...Path:%s...Upath:%s\n",
               hhd->qs, hhd->ext, hhd->file, hhd->path, hhd->upath);
               */
        return 0;
}

_http_header_data *parse_header(char *buf)
{
        int i, size, j, b, t;
	int content_len = 0;
        char line[HTTP_LINE_SIZE];
	char len[12];
        _http_header_data *hhd;
        
        if ((hhd = (_http_header_data *) malloc(sizeof(_http_header_data))) ==
	    NULL) {
                perror("Can not alloc http header_data\n");
                return NULL;
        }

        const char *k;

        size = strlen(buf);
        b = t = j = 0;
        memset(line, 0, HTTP_LINE_SIZE);

        for (i = 0; i < size; i++) {
                if (buf[i] < 32 && buf[i] > 126)
                        continue;

                if (i < (size - 1) && buf[i] == '\r' && buf[i + 1] == '\n') {
                        ++b;
                        continue;
                }

                if (b == 1) {
			//printf("%d, %s\n", content_len, line);
			b = 0; j = 0;

                        /* Parsing method etc. */
                        if ((k = strstr(line, "GET")) != NULL ||
                            (k = strstr(line, "POST")) != NULL) {
                                sscanf(k, "%s %s %s",
                                       hhd->method, hhd->path, hhd->proto);
                                parse_uri(hhd);
                        } else if ((k = strstr(line, "User-Agent:")) != NULL) {
                                k = strstr(line, ":");
                                snprintf(hhd->agent,
                                         MIN(strlen(line), HTTP_PARAM_SIZE - 1),
                                         "%s", k + 2);
                        } else if ((k = strstr(line, "Accept:")) != NULL) {
                                k = strstr(line, ":");
                                strncpy(hhd->accept, k + 2, HTTP_PARAM_SIZE - 1);
                        } else if ((k = strstr(line, "Host:")) != NULL) {
                                k = strstr(line, ":");
                                strncpy(hhd->host, k + 2, HTTP_PARAM_SIZE - 1);
                        } else if ((k = strstr(line, "Connection:")) != NULL) {
                                k = strstr(line, ":");
                                strncpy(hhd->connection, k + 2,
					HTTP_PARAM_SIZE - 1);
                        } else if ((k = strstr(line, "Content-Length:")) != NULL) {
				k = strstr(line, ":");
				strncpy(len, k + 2, 11);
				content_len = atoi(len);
			} else if ((k = strstr(line, "Content-Type:")) != NULL) {
                                k = strstr(line, ":");
                                strncpy(hhd->content_type, k + 2, HTTP_PARAM_SIZE - 1);
                        }
                        
                        memset(line, 0, HTTP_LINE_SIZE);
                        continue;

                } else if (b == 2) {
			b = j = 0;
			t = 1;
			memset(line, 0, HTTP_LINE_SIZE);
			continue;
                }

                if (j < HTTP_LINE_SIZE) {
                        line[j++] = buf[i];
                }

                /* POST data */
		if (i == (size - 1) &&
		    content_len > 0 &&
		    strcmp(line, "\r\n") != 0 &&
		    strstr(line, ":") == NULL) {
                        if (content_len > HTTP_MAX_POST_SIZE)
                                content_len = HTTP_MAX_POST_SIZE;
                        memset(hhd->post, 0, content_len);
                        strncpy(hhd->post, line, content_len - 1);
		}
        }

        return hhd;
}

static char *http_msg(int code)
{
        switch (code) {
                case 404:
                        return HTTP_MSG_404;
                case 403:
                        return HTTP_MSG_403;
                default:
                        return HTTP_MSG_DEFAULT;
        };
}

static int http_header(int fd, int code, char *content_type,
		       char *charset, int length)
{
        char time_str[32] = "0";
        time_t t;
        struct tm *tmp;

        if ((t = time(NULL)) != -1 &&
            (tmp = localtime(&t)) != NULL
            && strftime(time_str, sizeof(time_str), "%a, %d %b %Y %H:%M:%S %z", tmp));

        char header[HTTP_HEADER_SIZE];
        char *cstr = "";
        switch (code) {
                case 200:
                        cstr = HTTP_HEADER_200;
                        break;
                case 404:
                        cstr = HTTP_HEADER_404;
                        break;
                case 403:
                        cstr = HTTP_HEADER_403;
                        break;

                default:
                        cstr = HTTP_HEADER_400;
                        break;
        }

        snprintf(header,
                 sizeof(header) - 1,
                 "%s\r\n"
                 "Date: %s\r\n"
                 "Server: %s\r\n"
                 "Content-Length: %d\r\n"
                 "Content-Type: %s%s\r\n"
                 "Connection: closed\r\n\r\n",
                 cstr, 
                 time_str,
                 TD_NAME,
                 length,
                 content_type,
                 charset);
        send(fd, header, strlen(header), 0);

        return 0;
}

/* Send error html or msg to client */
static int http_method_err(int fd, int code, char *ext,
                           _http_header_data *hhd, char *ip)
{
        struct stat sb;
        char filename[1024];
        char *msg;

        snprintf(filename, 1023, "%s/%s/%s/err-%d.%s", root_dir, hhd->host,
                 HTTP_ERR_FILE_DIR, code, ext);
        log2file(hhd->host, hhd->method, hhd->ppath, hhd->proto, code, "",
                 0, hhd->agent, ip);

        msg = http_msg(code);
        int msg_size = strlen(msg);
 
        /* Error file is missing? */
        if (stat(filename, &sb) == -1) {
                http_header(fd, code, "text/html",
                            "utf-8", msg_size);
                send(fd, msg, msg_size, 0);
                return 0;
        }

        /* Send error file */
        http_header(fd, code, "text/html",
                    "utf-8", sb.st_size);

        net_sendfile(fd, filename, 0, sb.st_size);
        return 0;
}

/* Serve http get method */
static int http_method_get(_epoll_data *epoll_data)
{
        char filename[1024];
        char *idx;
        _http_header_data *hhd = (_http_header_data *)epoll_data->hhd;
        mime_set *mime_info;
        char mime[64] = "text/html";

        /* Get mime info by ext */
        /* printf("%s\n", hhd->ext); */
        if (hhd->ext && (mime_info = get_mimetype(hhd->ext)))
                snprintf(mime, 63, mime_info->mime);

         printf("%s\n", mime); 
        if ((idx = strchr(hhd->host, ':')) != NULL)
                hhd->host[idx - hhd->host] = '_';

        struct stat sb;

        snprintf(filename, 1023, "%s/%s/%s/%s%s%s", root_dir,
                 hhd->host, hhd->path, hhd->file,
                 hhd->ext ? "." : "",
                 hhd->ext ? hhd->ext : "");
        printf("Get file: %s\n, %s\n", filename, hhd->file);

        if (stat(filename, &sb) != -1) {
                printf("%d\n", sb.st_size);
                log2file(hhd->host, hhd->method, hhd->ppath, hhd->proto,
                         200, "", sb.st_size, hhd->agent, epoll_data->ip);
                http_header(epoll_data->fd, 200, mime,
                            strcmp(mime, "text/html") == 0 ? ";charset=utf-8" : "",
                            sb.st_size);
                if (net_sendfile(epoll_data->fd, filename, 0, sb.st_size) != 0){
                        fprintf(stderr, "Can not send file: %s\n",
                                filename);
                        return -1;
                }

                return 0;
        }

        if (strcmp(hhd->file, "index.html") == 0)
                http_method_err(epoll_data->fd, 403, "html", hhd, epoll_data->ip);
        else
                http_method_err(epoll_data->fd, 404, "html", hhd, epoll_data->ip);

        return 0;
}

int http_proto(_epoll_data *epoll_data)
{
        int err = 0, fd, size;

        _http_header_data *hhd = (_http_header_data *)epoll_data->hhd;

        if (!epoll_data || !hhd) {
                return -1;
        }

        if (!hhd->method) {
                /* Bad method */
                err = -1;
                http_header(epoll_data->fd, 400, "text/html",
                            "utf-8", strlen(HTTP_MSG_400));
                send(epoll_data->fd, HTTP_MSG_400,
                     strlen(HTTP_MSG_400), 0);
        } else if (strcmp(hhd->method, "GET") == 0) {
                /* GET method */
                err = http_method_get(epoll_data);
        } else if (strcmp(hhd->method, "POST") == 0) {
                /* POST method */
                http_header(epoll_data->fd, 200, "text/html", "utf-8",
                            strlen(hhd->post));
                if (send(epoll_data->fd, hhd->post,
                         strlen(hhd->post), 0) <= 0) {
                        err = -1;
                }
        } else {
                /* 400 */
                size = strlen(HTTP_MSG_400);

                http_header(epoll_data->fd, 400,
                            "text/html", "utf-8",
                            strlen(HTTP_MSG_400));

                if (send(epoll_data->fd, HTTP_MSG_400, size, 0) <= 0) {
                        perror("send error\n");
                        err = -1;
                }
        }

        fd = epoll_data->fd;
        del_event(epoll_data);

        /* Closing socket */
        close(fd);

        /* Free _http_header_data */
        free(epoll_data->hhd);
        free(epoll_data);

        return (err ? -1 : 0);
}

