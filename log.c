#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "log.h"

extern char *root_dir;
char log_access_file[1024] = "";

int log2file(char *host, char *method, char *path, char *proto,
             int code, char *refer,
             int size, char *agent, char *clientip)
{
        static int access_fd = -1;
        char line[LOG_FILE_LINE_SIZE];

        char time_str[32] = "0";
        time_t t;
        struct tm *tmp;

        if ((t = time(NULL)) != -1 &&
            (tmp = localtime(&t)) != NULL
            && strftime(time_str, sizeof(time_str), "%d/%b/%Y:%H:%M:%S %z", tmp));

        if (strcmp(log_access_file, "") == 0) {
                snprintf(log_access_file, 1023, "%s/%s/%s-%s",
                         root_dir, LOG_DIR, host, LOG_FILE_ACCESS);
        }

        if (access_fd == -1 &&
            (access_fd = 
             open(log_access_file,
                  O_APPEND | O_WRONLY | O_CREAT /*| O_LARGEFILE*/, 0644)) == -1) {
                fprintf(stderr, "Can not open log file: %s\n", log_access_file);
                return -1;
        }
        snprintf(line, LOG_FILE_LINE_SIZE - 1,
                 "%s - - [%s] \"%s %s %s\" %d %d \"%s\" \"%s\"\n",
                 clientip, time_str, method, path, proto, code,
                 size, refer, agent);

        if (access_fd == -1)
                return -1;
        if (write (access_fd, line, strlen(line)) == -1) {
                fprintf(stderr, "Can not write to log file: %s\n",
                        log_access_file);
                return -1;
        }

        return 0;
}
