#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include "net.h"
#include "http.h"
#include "event.h"

int epfd;

extern int threads;

static _epoll_data *new_epoll_data(_epoll_data *epoll_data);

static void *main_poll_handler(void *p)
{
        int i, nfds;
        struct epoll_event events[EVENTSIZE];
        char header_buf[HTTP_HEADER_SIZE];
        _epoll_data *epoll_data;

        for (;;) {
                nfds = epoll_wait(epfd, events, EVENTSIZE , -1);

                for (i = 0; i < nfds; i++) {
                        if (events[i].events & EPOLLIN) {
                                epoll_data = (_epoll_data*) events[i].data.ptr;

                                if (recv(epoll_data->fd, header_buf,
                                         HTTP_HEADER_SIZE - 1, MSG_WAITALL)
                                    == -1)
                                        continue;

                                handle_input_event(epoll_data, header_buf);

                        } else if(events[i].events & EPOLLOUT) {
                                handle_output_event((_epoll_data *)events[i].data.ptr);
                        }
                }
        }
        return NULL;
}

int main_poll_loop(int socket_fd)
{
        int cfd;
        pthread_attr_t attr;
        pthread_t thread_id;
        struct sockaddr_in cin;
        socklen_t cin_len = sizeof(struct sockaddr);
        struct epoll_event ev;
        _epoll_data *epoll_data;

        if ((epfd = epoll_create(MAXFDS)) == -1) {
                perror("Can not create epoll instance.\n");
                exit (-1);
        }

        if (pthread_attr_init(&attr) !=0) {
                perror("pthread_attr_init error.\n");
                exit (-1);
        }

        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

        int i = 0;

        printf("%d\n", threads);
        for (i = 0; i < threads; i++) {
                if (pthread_create(&thread_id, &attr, &main_poll_handler, NULL) != 0) {
                        perror("pthread_create failed\n");
                        return -1;
                }
        }

        while ((cfd = accept(socket_fd, (struct sockaddr *)&cin, &cin_len)) > 0) {
                setnonblocking(cfd);

                if ((epoll_data = new_epoll_data(epoll_data)) == NULL)
                        continue;

                /* client socket */
                epoll_data->fd = cfd;
                /* client ip address */
                inet_ntop(AF_INET, &cin.sin_addr.s_addr,
                          (char *)epoll_data->ip, IP_STRSIZE);
                /* file read pos */
                epoll_data->fpos = 0;
                ev.data.ptr = epoll_data;
                ev.events = EPOLLIN | EPOLLET | EPOLLONESHOT;
                epoll_ctl(epfd, EPOLL_CTL_ADD, cfd, &ev);
        }

        if (socket_fd > 0)
                close(socket_fd);

        return 0;
}

int handle_input_event(_epoll_data * epoll_data, char *raw_head)
{
        struct epoll_event ev;

        if ((epoll_data->hhd = (_http_header_data *) parse_header(raw_head)) == NULL) {
                epoll_data->fd = -1;
                return -1;
        }

        epoll_data->fpos = 0;
        ev.data.ptr = epoll_data;
        ev.events = EPOLLOUT | EPOLLET | EPOLLONESHOT;
        epoll_ctl(epfd, EPOLL_CTL_MOD, epoll_data->fd, &ev);

        return 0;
}

int handle_output_event(_epoll_data *epoll_data)
{
        return http_proto(epoll_data);
}

int del_event(_epoll_data *epoll_data)
{
        struct epoll_event ev;

        ev.data.ptr = NULL;
        ev.events = 0;
        epoll_ctl(epfd, EPOLL_CTL_DEL, epoll_data->fd, &ev);
        return 0;
}

static _epoll_data *new_epoll_data(_epoll_data *epoll_data)
{
        if ((epoll_data = (_epoll_data *) malloc(sizeof(_epoll_data))) == NULL) {
                perror("epoll_data error\n");
                return NULL;
        }

        return epoll_data;
}
