#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "mime.h"
#include "str.h"

mime_set *_g_mime_set = NULL;

int init_mime_set()
{
        FILE *fp;
        char line[512];
        char mime[64];
        char _key[32];
        char *key = NULL;
        char *saveptr;
        int size, id = 0;
        mime_set *_mime;

        if ((fp = fopen(MIME_TYPE_FILE, "r")) == NULL) {
                printf("Could not read Mime type file: %s\n", MIME_TYPE_FILE);
                exit (-1);
        }

        while (fgets(line, 511, fp)) {
                if (line[0] == '#' || line[0] == '\n' || strlen(line) == 1)
                        continue;

                memset(_key, 0, 32);
                memset(mime, 0, 64);

                sscanf(line, "%64s\t%32[^n]", mime, _key);

                size = strlen(_key);
                if (size <= 1)
                        continue;
                if (_key[size - 1] == '\n')
                        _key[size - 1] = '\0';

                if (strchr(_key, ' ') != NULL) {
                        key = strtok_r(_key, " ", &saveptr);
                        while (key != NULL) {
                                if ((_mime = (mime_set *)malloc(sizeof(mime_set))) == NULL) {
                                        fprintf(stderr, "Could not alloc memory for mime!");
                                        exit (-1);
                                }
                 //               printf(":->%s,\n", key);
                                strncpy(_mime->key, key, sizeof(_mime->key) - 1);
                                strncpy(_mime->mime, mime, sizeof(_mime->mime) - 1);
                                _mime->id = id++;
                                HASH_ADD_STR(_g_mime_set, key, _mime);
                                key = strtok_r(NULL, " ", &saveptr);
                        }
                //        printf("end.\n");
                        continue;
                }

                if ((_mime = (mime_set *)malloc(sizeof(mime_set))) == NULL) {
                        fprintf(stderr, "Could not alloc memory for mime!");
                        exit (-1);
                }
                strncpy(_mime->key, _key, sizeof(_mime->key) - 1);
                strncpy(_mime->mime, mime, sizeof(_mime->mime) - 1);
                _mime->id = id++;
                key = _key;
                HASH_ADD_STR(_g_mime_set, key, _mime);
        }

        fclose(fp);

        return 0;
}

mime_set * get_mimetype(char *key)
{
        mime_set *_mime;
        HASH_FIND_STR(_g_mime_set, key, _mime);

        return _mime;
}
