#include <signal.h>

void sig_handler()
{
        signal(SIGPIPE, SIG_IGN);
        signal(SIGCHLD, SIG_IGN);
}
