#ifndef EVENT_INCLUDED
#define EVENT_INCLUDED

#define MAXFDS          1024 /* Unused for epoll_create() */
#define EVENTSIZE       300000 /* epoll event size */
#define IP_STRSIZE      14

typedef struct {
        int fd;
        void *hhd;
        char ip[IP_STRSIZE];
        int stat;
        off_t fpos; /* offset of sended file */
} _epoll_data;

int main_poll_loop(int socket_fd);
int handle_input_event(_epoll_data * epoll_data, char *raw_haed);
int handle_output_event(_epoll_data *epoll_data);
int del_event(_epoll_data *epoll_data);

#endif
